module.exports = grunt => {
    grunt.initConfig({
        concat: {
            options: {
                sourceMap: true
            },
            "js": {
                src: ["src/js/*.js"],
                dest: "web/gen/app.concat.js"
            },
        },

        exec: {
            build: "D:/code/emsdk/emsdk_env.bat & echo Building... & cd embuild/ & emmake make & make install & cd ../",
            cmake: "cd embuild/ && cmake .. && cd ../"
        },

        watch: {
            cmake: {
              files: ["CMakeLists.txt"],
              tasks: ["exec:cmake", "exec:build"]
            },
            cpp: {
                files: ["src/cpp/*.cpp", "src/cpp/*.h"],
                tasks: ["exec:build"]
            },
            js: {
                files: ["src/js/*.js"],
                tasks: ["concat"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-exec");

    grunt.registerTask("default", ["watch"]);
    grunt.task.run(["exec:cmake", "exec:build"]);
  };
