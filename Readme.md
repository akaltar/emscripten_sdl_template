For development run these in two terminals.
Grunt responsible to recompile cpp when changes occur,
light-server serves the content to `localhost:8080`
```
npm install -g grunt-cli
npm install
grunt
```

And in another console:
```
npm run serve
```
