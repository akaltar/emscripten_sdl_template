cmake_minimum_required (VERSION 3.7)
project (emtest)

# Specifies the C++ version, using latest widely supported C++14
set( CMAKE_CXX_STANDARD 14 )
set( CMAKE_CONFIGURATION_TYPES "Debug;Release" )

if(${CMAKE_SYSTEM_NAME} MATCHES Linux)
    set(PLATFORM_LINUX TRUE)
endif()
if(${CMAKE_SYSTEM_NAME} MATCHES Darwin)
    set(PLATFORM_MACOSX TRUE)
endif()
if(${CMAKE_SYSTEM_NAME} MATCHES Windows)
    set(PLATFORM_WINDOWS TRUE)
endif()

if( PLATFORM_LINUX )
	# Force building with debug symbols on linux.
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
endif()

if(MSVC)
	# Statically link the runtime.
    add_compile_options(
        $<$<CONFIG:RelWithDebInfo>:/MT>
        $<$<CONFIG:Debug>:/MTd>
        $<$<CONFIG:Release>:/MT>
    )

	# Enable multicore compilation.
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")

	# Manually make the Release target have debug info instead of using
	# RelWithDebInfo since that is ugly, long and isn't supported by vcpkg properly.
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /Zi")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} /DEBUG")
endif()

if(EMSCRIPTEN)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -s USE_SDL=2")
else()
    find_package(SDL2 REQUIRED)
endif()

# Find dependencies that have .cmake files
#find_package(OpenGL REQUIRED)

#include_directories( ${FMOD_INCLUDE_DIRS} ${CEF_INCLUDE_DIRS}
#	${ASSIMP_INCLUDE_DIRS} ${BULLET_INCLUDE_DIRS} ${LUA_INCLUDE_DIRS}
#	sunshine_core/ sunshine_glfrontend/ sunshine_luaapi/ )

#link_directories( ${FMOD_LIBRARY_DIRS} ${CEF_LIBRARY_DIRS} ${ZLIB_LIBRARY_DIRS}
#	${ASSIMP_LIBRARY_DIRS} ${BULLET_LIBRARY_DIRS} ${LUA_LIBRARY_DIRS}  )


# Collect sources with glob so we can add other files without changing CMakeLists.txt
# as long as they are included by one of the globs.
file( GLOB SOURCES src/cpp/* )

add_executable( emtest ${SOURCES} )
#target_link_libraries( emtest SDL2::SDL2main SDL2::SDL2-static )

# Adds the libraries to link against for the application
#target_link_libraries( sunshine_game ${ZLIB_LIBRARIES} SDL2::SDL2main SDL2::SDL2-static unofficial::liquidfun::LiquidFun
#	$<$<CONFIG:RelWithDebInfo>:IrrXML>
#	$<$<CONFIG:Release>:IrrXML>
#	$<$<CONFIG:Debug>:IrrXMLd>
#	${ASSIMP_LIBRARIES} ${BULLET_LIBRARIES} ${LUA_LIBRARIES} ${FMOD_LIBRARIES} ${CEF_LIBRARIES} )

if(MSVC)
	#target_link_libraries( emtest ws2_32 )
	set_target_properties( emtest PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/Release/x64/" )
endif()

set( OUTPUT_DIR "${CMAKE_SOURCE_DIR}/web/gen/" )

if(EMSCRIPTEN)
    install( DIRECTORY "${CMAKE_BINARY_DIR}/"
        DESTINATION ${OUTPUT_DIR}
        FILES_MATCHING PATTERN "emtest.*"
        PATTERN "CMakeFiles*" EXCLUDE )
endif()
