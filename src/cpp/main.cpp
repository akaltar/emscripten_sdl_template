#include <iostream>
#include "SDL.h"
#include "emscripten.h"

SDL_Window* mWindow = nullptr;
SDL_Renderer* mRenderer = nullptr;

extern "C" void render()
{

	SDL_SetRenderDrawColor( mRenderer, rand() & 0xFF, 50, 50, 255 );
	SDL_RenderClear( mRenderer );
	SDL_RenderPresent( mRenderer );
}


extern "C" int main( int argc, char** argv )
{
	std::cout << "init" << std::endl;
	if( SDL_Init( SDL_INIT_VIDEO ) )
	{
		std::cout << "Error initing." << SDL_GetError();
		return -1;
	}

	SDL_DisplayMode DisplayMode;
	const int DisplayIndex = 0;
	SDL_GetCurrentDisplayMode( DisplayIndex, &DisplayMode );

	mWindow = SDL_CreateWindow( "test",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		//800, 800,  SDL_WINDOW_SHOWN );
		DisplayMode.w, DisplayMode.h, 0 );
	if( !mWindow )
	{
		std::cout << "Error creating SDL_Window: " << SDL_GetError();
		return -1;
	}
	std::cout << "Created window" << std::endl;

	mRenderer = SDL_CreateRenderer( mWindow, -1, 0 );
	if( !mRenderer )
	{
		std::cout << "Error creating renderer: " << SDL_GetError();
		return -1;
	}
	std::cout << "Created renderer" << std::endl;

	emscripten_set_main_loop( &render, 60, 1 );
	return 0;

}

